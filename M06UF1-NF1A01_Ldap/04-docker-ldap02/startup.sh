#! /bin/bash
/usr/bin/echo "Preparant la BD slapd"
/bin/bash /opt/docker/startup-salpd.sh  && echo "ok"

/usr/bin/echo "starting slapd..."
/usr/sbin/slapd -u ldap -h "ldap:/// ldaps:/// ldapi:///" && echo "ok"

/usr/bin/echo "starting sshd-keygen"
/usr/sbin/sshd-keygen && echo "ok"

/usr/bin/echo "starting sshd"
#/usr/sbin/sshd -D $OPTIONS && echo "ok"
/usr/sbin/sshd && echo "ok"

/usr/bin/echo "starting nscd"
/usr/sbin/nscd && echo "ok"

/usr/bin/echo "starting nslcd"
/usr/sbin/nslcd && echo "ok"

