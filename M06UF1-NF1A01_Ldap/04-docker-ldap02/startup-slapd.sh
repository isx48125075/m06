#! /bin/bash
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
rm -rf /etc/openldap/slapd.d/*
slaptest -f /opt/edt-ldap/slapd-edt.org.acl.conf -F /etc/openldap/slapd.d
slapadd -F /etc/openldap/slapd.d -l /opt/edt-ldap/organitzacio_edt.org.ldif
slapadd -F /etc/openldap/slapd.d -l /opt/edt-ldap/usuaris-edt.org.ldiff
chown -R ldap.ldap /etc/openldap/slapd.d/
chown -R ldap.ldap /var/lib/ldap/
/usr/sbin/slapd -u ldap -h "ldap:/// ldaps:/// ldapi:///" && echo "ok"
/bin/bash

