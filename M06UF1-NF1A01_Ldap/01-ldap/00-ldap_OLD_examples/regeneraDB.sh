#! /bin/bash
# regenerar la BD de slapd.d
# $1 fitxer de configuracio a usar

systemctl stop slapd
# rm -rf /var/lib/ldap/*
# rm -rf /var/lib/ldap.m06.cat
# cp /usr/share/openldap-servers/DB_CONFIG.example    /var/lib/ldap/DB_CONFIG
rm -rf /etc/openldap/slapd.d/*
slaptest -v -f $1 -F /etc/openldap/slapd.d
chown -R ldap.ldap /var/lib/ldap
chown -R ldap.ldap /var/lib/ldap.m06.cat
chown -R ldap.ldap /etc/openldap/slapd.d
systemctl start slapd

