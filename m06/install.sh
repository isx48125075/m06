#! /bin/bash
#descripcio de la creacio de un server ldap

docker run --name ldap02 --hostname -it fedora:27 /bin/bash

dnf -y install procps vim iputils iproute tree nmap mlocate man less

dnf -y install openldap-clients openldap-servers

mkdir /docker

docker cp /var/tmp/m06/* ldap02:/docker

#passos instalacio slapd
rm -rf /etc/openldap/slapd.d/*
rm -rf /var/lib/ldap/*

slaptest -f slapd-edt.org.conf -u
slaptest -v -f slapd-edt.org.conf -F /etc/openldap/slapd.d

slapadd -v -F /etc/openldap/slapd.d -l organitzacio_edt.org.ldif #cada cop q es fa una ordre slap s'han de canviar els permisos

chown -R ldap.ldap /etc/openldap/slapd.d
chown -R ldap.ldap /var/lib/ldap

slapcat #mira directament els fitxers pq el dimoni esta apagat

/sbin/slapd
ldapsearch -x -LLL -b 'dc=edt,dc=org' dn
ldapadd -x -h 172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret -f usuaris_edt_org.ldif
