Docker ldap
===========

Versions de docker
------------------

 * ldap10 servidor ldap de la BD "edt.org"
 * ldap20 autenticació PAM + ldap (bd "edt.org")
 * ldap30 BD ldap amb groups
 * ldap40 BD ldap amb hosts

Altres versions de docker
-------------------------

 * cups10 servidor cups amb impressores
 * samba10 servidor amb samba shares (no PDC)
 * samba20 servidor samba funcionant de PDC
 * samba30 servidor samba member (no PDC)

Docker Manual
=============

### Per  consultar aquesta ajuda executa: 
 $ documentation

### Per consultar ajuda de funcionament executa_
 $ manual 


### Per executar els serveis de xarxa fer:
equivalent a fer systemctl
 $ startup

