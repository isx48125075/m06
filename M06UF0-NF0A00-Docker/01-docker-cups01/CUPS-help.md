LDAP HELP
=========

## Help descriptiu de l'accés al servidor ldap

 1. Observar el contingut de la BD amb un volcat _slapcat_

    [container$ slapcat

 2. Llistar tots els DN de la BD amb _ldapsearch_

    [container]$ ldapsearch -x -LLL -h localhost -b 'dc=edt,dc=org' dn

 3. Llistar tots els usuaris de la família _Pou_

    [container]$ ldapsearch -x -LLL -h localhost -b 'ou=usuaris,dc=edt,dc=org' 'cn=* Pou'

 4. Mostra una entrada concreta per _uid_

    [container]$ ldapsearch -x -LLL -h localhost -b 'ou=usuaris,dc=edt,dc=org' -s one-s one  'uid=anna' 

## Des d'un host extern accedir al servei del container docker

 * Identificar l'adreça IP del container docker
    
    $ docker inspect | grep "IPAddress"

 * Consultar la BD del container

  * Mostrar tota la BD

    $ ldapsearch -x -LLL -h <IPAdress>t -b 'dc=edt,dc=org' 

  * Mostrar tots els DN dels usuaris

    $ ldapsearch -x -LLL -h localhost -b 'ou=usuaris,dc=edt,dc=org' dn

 
