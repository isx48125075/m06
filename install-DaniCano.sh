#! /bin/bash
# descripcio de la creació de un server ldap

#docker run --name ldap02 -hostname ldap02 -it fedora:27 ldap02 /bin/bas
dnf -y install procps man iputils iproute less nmap tree mlocate openldap-clients openldap-servers vim
mkdir /docker 

# docker cp /var/tmp/m06/01-ldap/* ldap02:/docker

# Passos instal·lació slapd:
rm -rf /etc/openldap/slapd.d/*  # configuracio
rm -rf /var/lib/ldap/*		# BD
vim slapd-edt.org.conf		# editem si fa falta
slaptest -uf slapd-edt.org.conf # verificar la sintaxi del fitxer
slaptest -vf slapd-edt.org.conf -F /etc/openldap/slapd.d/ # generar/planxa la configuacio al directori
slaptest -uvf slapd-edt.org.conf -F /etc/openldap/slapd.d/ #
slaptest -uv -F /etc/openldap/slapd.d/ # verificar el directori

# Carrega de dades a baix nivell
slapadd -v -F /etc/openldap/slapd.d/ -l organitzacio_edt.org.ldif

# Canviem els permisos en el directori de conf i en el de BD
chown -R ldap.ldap /etc/openldap/slapd.d/
chown -R ldap.ldap /var/lib/ldap/        

# Engeguem el dimoni 
/etc/slapd

ps ax
nmap localhost

# Veure dades
ldapsearch -x -LLL -h localhost -b 'dc=edt,dc=org' dn # dins del docker
ldapsearch -x -LLL -h 172.17.0.2 -b 'dc=edt,dc=org' dn # fora del docker

# Carrega de dades a alt nivell
ldapadd -x -h -D 'cn=Manager,dc=edt,dc=org' -w secret -f usuaris_edt.org.ldi # dins del docker
ldapadd -x -h 172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -w secret -f usuaris_edt.org.ldi # fora del docker
